<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           30/10/2017
 * @project        GYGInterview
 * @package        App\Command
 */

namespace App\Command;

use App\Handler\AvailableProductsHandler;
use App\Handler\AvailableProductsHandlerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Available Products Command
 *
 * @property ValidatorInterface validator
 * @package App\Command
 */
class AvailableProductsCommand extends Command
{
    /**
     * @var AvailableProductsHandlerInterface
     */
    protected $handler;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * AvailableProductsCommand constructor.
     * @param ValidatorInterface $validator
     * @param AvailableProductsHandlerInterface $handler
     */
    public function __construct(ValidatorInterface $validator, AvailableProductsHandlerInterface $handler)
    {
        parent::__construct();

        $this->handler = $handler;
        $this->validator = $validator;
    }

    /**
     * @inheritdoc
     */
    public function configure()
    {
        $this
            ->setName('products:available')
            ->setDescription('Fetch available products based on the given arguments')
            ->addArgument('url', InputArgument::REQUIRED, 'API Endpoint - The full url to the api endpoint')
            ->addArgument('start_time', InputArgument::REQUIRED, 'The start of the time period requested in the following format: “Y-m-d\\TH:i” e.g. 2017-11-23T19:30.')
            ->addArgument('end_time', InputArgument::REQUIRED, 'The end of the time period in the same format as the start_datetime')
            ->addArgument('travelers_count', InputArgument::REQUIRED, 'Number of travelers');
    }

    /**
     * @inheritdoc
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $apiURL = $input->getArgument('url');
        $startTime = $input->getArgument('start_time');
        $endTime = $input->getArgument('end_time');
        $numberOfTravelers = $input->getArgument('travelers_count');

        $this->validate([
            'api_url' => $apiURL,
            'start_time' => $startTime,
            'end_time' => $endTime,
            'travelers_count' => $numberOfTravelers,
        ]);

        $result = $this->handler->importAvailableProducts($apiURL, $startTime, $endTime, $numberOfTravelers);

        $output->writeln(json_encode($result, true));
    }

    protected function validate(array $inputs)
    {
        // @TODO one more validator start date is less than end date
        $rules = [
            'api_url' => [new NotBlank(), new Url(),],
            'start_time' => [new NotBlank(), new DateTime(['format' => AvailableProductsHandler::DATE_TIME_FORMAT]),],
            'end_time' => [new NotBlank(), new DateTime(['format' => AvailableProductsHandler::DATE_TIME_FORMAT]),],
            'travelers_count' => [new NotBlank(), new Range(['min' => 1, 'max' => 30])],
        ];

        foreach ($inputs as $name => $value) {
            $errors = $this->validator->validate($value, $rules[$name]);

            if (count($errors) > 0) {
                throw new \InvalidArgumentException(sprintf("Argument (%s) has error(s) %s", $name, (string)$errors));
            }
        }
    }
}
