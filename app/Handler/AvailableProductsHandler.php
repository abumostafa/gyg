<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           30/10/2017
 * @project        GYGInterview
 * @package        App\Handler
 */

namespace App\Handler;

use Carbon\Carbon;
use GuzzleHttp\ClientInterface;

/**
 * Available Products Handler
 *
 * @package App\Handler
 */
class AvailableProductsHandler implements AvailableProductsHandlerInterface
{
    const DATE_TIME_FORMAT = 'Y-m-d\\TH:i';

    /**
     * @var ClientInterface
     */
    private $httpClient;
    /**
     * @var Carbon
     */
    private $timeManager;

    /**
     * AvailableProductsHandler constructor.
     * @param ClientInterface $httpClient
     * @param Carbon $timeManager
     */
    public function __construct(ClientInterface $httpClient, Carbon $timeManager)
    {

        $this->httpClient = $httpClient;
        $this->timeManager = $timeManager;
    }

    /**
     * @param string $url
     * @param string $startDate
     * @param string $endDate
     * @param int $travelersCount
     * @return array
     * @throws \Exception
     */
    public function importAvailableProducts($url, $startDate, $endDate, $travelersCount)
    {
        $startDate = Carbon::createFromFormat(self::DATE_TIME_FORMAT, $startDate);
        $endDate = Carbon::createFromFormat(self::DATE_TIME_FORMAT, $endDate);

        $response = $this->httpClient->request('GET', $url);
        $products = json_decode($response->getBody()->getContents(), true);

        if (json_last_error()) {
            throw new \Exception(json_last_error_msg());
        }

        $availableProducts = $this->filterProducts($products['product_availabilities'], $startDate, $endDate, $travelersCount);
        $availableProducts = $this->sortProductByProductId($availableProducts);
        $availableProducts = $this->createProductStartTimes($availableProducts, $endDate);

        return $availableProducts;
    }

    /**
     * @param array $products
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @param int $travelersCount
     * @return array
     */
    protected function filterProducts($products, $startDate, $endDate, $travelersCount)
    {
        if (0 === count($products)) {
            return $products;
        }

        return array_filter($products, function ($product) use ($startDate, $endDate, $travelersCount) {

            $activityStartDate = Carbon::createFromFormat(self::DATE_TIME_FORMAT, $product['activity_start_datetime']);
            $firstActivityEndDate = Carbon::createFromFormat(self::DATE_TIME_FORMAT, $product['activity_start_datetime'])
                ->addMinutes($product['activity_duration_in_minutes']);

            return (
                $product['places_available'] >= $travelersCount &&
                $activityStartDate->greaterThanOrEqualTo($startDate) &&
                $firstActivityEndDate->lessThanOrEqualTo($endDate)
            );
        });
    }

    /**
     * @param $products
     * @return mixed
     */
    protected function sortProductByProductId(array $products)
    {
        usort($products, function ($a, $b) {
            return $a['product_id'] > $b['product_id'];
        });

        return $products;
    }

    /**
     * @param array $products
     * @param Carbon $endDate
     * @return array
     */
    protected function createProductStartTimes(array $products, Carbon $endDate)
    {
        return array_map(function ($product) use ($endDate) {

            // st time
            // duration
            // time + duration <= end
            $activityEndDate = Carbon::createFromFormat(self::DATE_TIME_FORMAT, $product['activity_start_datetime'])->addMinutes($product['activity_duration_in_minutes']);

            $startTimes = [
                $product['activity_start_datetime']
            ];

            while ($endDate->greaterThanOrEqualTo($activityEndDate)) {
                $startTimes[] = $activityEndDate->format(self::DATE_TIME_FORMAT);
                $activityEndDate->addMinutes($product['activity_duration_in_minutes']);
            }

            usort($startTimes, function ($a, $b) {
                $dateA = Carbon::createFromFormat(self::DATE_TIME_FORMAT, $a);
                $dateB = Carbon::createFromFormat(self::DATE_TIME_FORMAT, $b);

                return $dateA->greaterThanOrEqualTo($dateB);
            });

            return [
                'product_id' => $product['product_id'],
                'available_starttimes' => $startTimes,
            ];
        }, $products);
    }
}
