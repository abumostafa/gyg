<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           30/10/2017
 * @project        GYGInterview
 * @package        App\Handler
 */

namespace App\Handler;

/**
 * Interface AvailableProductsHandlerInterface
 * @package App\Handler
 */
interface AvailableProductsHandlerInterface
{
    /**
     * @param string $url
     * @param string $startDate
     * @param string $endDate
     * @param int $travelersCount
     */
    public function importAvailableProducts($url, $startDate, $endDate, $travelersCount);
}
