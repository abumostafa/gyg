<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           30/10/2017
 * @project        GYGInterview
 */

require __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\Console\Application;

$application = new Application();

$httpClient = new \GuzzleHttp\Client();
$carbon = new \Carbon\Carbon();

$validator = \Symfony\Component\Validator\Validation::createValidator();
$handler = new \App\Handler\AvailableProductsHandler($httpClient, $carbon);

$application->add(new \App\Command\AvailableProductsCommand($validator, $handler));

return $application;