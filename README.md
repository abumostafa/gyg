# GYG

###NOTE:
***```This code is not production ready. it's a concept of a command line tool to fetch available activities available```***

The GYG is a very simple CLI based on [The Console Component](https://symfony.com/doc/current/console.html) library and some other open source tools. 

####Libraries
- [The Console Component](https://symfony.com/doc/current/console.html)
- [The Validator Component](https://symfony.com/doc/current/components/validator.html)
- [GuzzleHttp](http://docs.guzzlephp.org/en/stable/index.html)
- [Carbon](http://carbon.nesbot.com/)

####Installation
- Install composer `curl -sS https://getcomposer.org/installer | php`
- Run `php composer.phar install`

####The Console
The console can support as many commands as you want. since it's just a basic example the console supports only one command at the moment.
to see the available commands please run ```php console``` or ```php console list```

####Examples
- ```php console products:available "http://www.mocky.io/v2/58ff37f2110000070cf5ff16" 2017-11-20T09:30 2017-11-23T19:30 3```
- to beatify the JSON response you can add ``` | python -m json.tool``` to the end of your command ```php console products:available "http://www.mocky.io/v2/58ff37f2110000070cf5ff16" 2017-11-20T09:30 2017-11-23T19:30 3 | python -m json.tool```

####How to improve this package
- It needs a proper dependency manager to handle the loading of the used services.
- Some Unit/Functional Tests needs to be add

